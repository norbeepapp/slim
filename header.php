<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php wp_site_icon(); ?>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header class="head">
  <div class="wrap">
  	<nav class="main-menu">
      <?php 
      if ( function_exists( 'the_custom_logo' )) {
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
      }
      if ( function_exists( 'the_custom_logo' ) && isset( $image[0] ) )
        the_custom_logo();
      else {
        ?>
        <div class="logo-holder"><a href="<?php esc_url( home_url() ); ?>" title="<?php bloginfo( 'sitename' ); ?>" class="site-title"><?php bloginfo( 'sitename' ); ?></a></div>
        <?php
      }
      ?>
      <input type="checkbox" id="menu-toggle">
        <label for="menu-toggle" class="label-toggle"></label>
  		<?php
  		$defaults = array(
  			'theme_location'  => 'main-menu',
  			'container'       => false,
  			'menu_class'      => 'menu',
  			'fallback_cb'     => 'wp_page_menu',
  			'depth'           => 3,
  		);
  		wp_nav_menu( $defaults ); 
  		?>
  	</nav>
  </div>
</header>

<main class="content">