<?php get_header(); ?>

<?php

// The Loop
while ( have_posts() ) : the_post(); ?>
  <article id="post-<?php the_ID(); ?>" <?php post_class( 'middle' ); ?>>
    <header>
      <h1><?php the_title(); ?></h1>
      <span><?php the_author(); ?> &#215; <time datetime="<?php echo get_the_date( 'c' ); ?>" pubdate><?php echo date( get_option( 'date_format', $timestamp ) ); ?></time> &#215; <?php echo get_the_category_list( __( ', ', 'slim' ) ); ?></span>
    </header>
    <?php
    if ( has_post_thumbnail() ) { ?>
      <figure class="featured-image">
        <?php
        the_post_thumbnail( 'full' );

        echo '<figcaption>';
        the_post_thumbnail_caption();
        echo '</figcaption>';
        ?>
      </figure>
      <?php
    }
    ?>
    <div class="post-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <div class="wrapper_tags"><?php the_tags(); ?></div>
      <figure>
        <figcaption></figcaption>
      </figure>
    </footer>
  </article>

  <?php
  endwhile;
  ?>
<?php wp_link_pages(); ?>
<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>