<?php get_header(); ?>
  <?php // single_cat_title('Currently browsing '); ?>
<?php
while ( have_posts() ) : the_post();
  get_template_part( 'templates/content', 'archive' );
endwhile;
?>
<?php wp_link_pages(); ?>
<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>