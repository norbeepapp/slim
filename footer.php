</main>
<footer class="footer">
  <ul class="footer-widgets">
  	<?php dynamic_sidebar('footer'); ?>
  </ul>
  <div class="copyright">
  	<?php 
      if ( function_exists( 'the_custom_logo' )) {
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
      }
      if ( function_exists( 'the_custom_logo' ) && isset( $image[0] ) )
        the_custom_logo();
      else {
        ?>
        <div class="logo-holder">
        	<a href="<?php esc_url( home_url() ); ?>" title="<?php bloginfo( 'sitename' ); ?>" class="site-title"><?php bloginfo( 'sitename' ); ?></a>
        </div>
        <?php
      }
      ?>
  	<p>&copy; 2018 Slim. All rights reserved.</p>
  </div>
</footer>
<?php wp_footer(); ?>

</body>
</html>